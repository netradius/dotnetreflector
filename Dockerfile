FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-alpine
WORKDIR /app
COPY out .
EXPOSE 8080
ENV ASPNETCORE_URLS "http://*:8080"
ENTRYPOINT ["dotnet", "dotnetreflector.dll"]
HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD curl --fail http://localhost:8080/health || exit 1