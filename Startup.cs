﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Primitives;

namespace dotnetreflector
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest);
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.XForwardedProto |
                                           ForwardedHeaders.XForwardedHost |
                                           ForwardedHeaders.XForwardedFor;
                // https://github.com/aspnet/IISIntegration/issues/140
                // https://github.com/aspnet/BasicMiddleware/issues/37
                // https://github.com/aspnet/BasicMiddleware/commit/420bb912dc95e0699c1389af42d5520063d484be
                options.RequireHeaderSymmetry = false;
                // https://github.com/aspnet/BasicMiddleware/issues/177
                options.KnownNetworks.Clear();
                options.KnownProxies.Clear();

            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseForwardedHeaders();
            app.UseRouting();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            app.Use((context, next) =>
            {
                var val = Environment.GetEnvironmentVariable("APPSETTING_USE_X_ORIGINAL_HOST");
                if ("true".Equals(val) 
                    && context.Request.Headers.TryGetValue("X-Original-Host", out StringValues host))
                {
                    context.Request.Host = new HostString(host); 
                }
                return next();
            });
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseHttpsRedirection();
        }
    }
}
