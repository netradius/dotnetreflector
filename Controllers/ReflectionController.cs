using System;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace dotnetreflector.Controllers
{
    [Route("")]
    public class ReflectionController : Controller
    {
        
        private IMemoryCache cache;

        public ReflectionController(IMemoryCache cache)
        {
            this.cache = cache;
        }
        
        [HttpGet("{*path}")]
        [HttpPost("{*path}")]
        public IActionResult Index(string path)
        {
            var sb = new StringBuilder()
                .Append("Path: ").Append(path).AppendLine()
                .Append("Method: ").Append(Request.Method).AppendLine()
                .Append("Protocol: ").Append(Request.Protocol).AppendLine()
                .Append("Is HTTPs: ").Append(Request.IsHttps).AppendLine()
                .Append("Scheme: " ).Append(Request.Scheme).AppendLine()
                .Append("Host: ").Append(Request.Host).AppendLine()   
                .Append("Remote IP: ").Append(HttpContext.Connection.RemoteIpAddress).AppendLine()
                .Append("Remote Port: ").Append(HttpContext.Connection.RemotePort).AppendLine()
                .Append("Headers:").AppendLine();
            
            foreach (var key in Request.Headers.Keys)
            {
                foreach (var val in Request.Headers[key])
                {
                    sb.Append("   ").Append(key).Append(": ").Append(val).AppendLine();
                }   
            }

            sb.Append("Parameters: ").Append(Request.Query.Count == 0 ? "None" : "").AppendLine();
            foreach (var key in Request.Query.Keys)
            {
                foreach (var val in Request.Query[key])
                {
                    sb.Append("    ").Append(key).Append(": ").Append(val).AppendLine();
                }   
            }

            var result = sb.Append("Serial: ").Append("11").AppendLine().ToString();
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromMinutes(60));
            cache.Set(path, result, cacheEntryOptions);
            return Content(result);
        }

        [HttpGet("/cached/{*path}")]
        public IActionResult Cache(String path)
        {
            var result = cache.Get(path);
            if (result != null)
            {
                return Content((string)result);
            }

            return NotFound();
        }

    }
    
}